# Petite animation


### Présentation 
>Travaille personnel.<br>
Animation à l'aide de sprites(Link, le héro de Zelda).



## Technologies utilisées

* HTML
* CSS
    * Bootstrap
* JavaScript



## Interets

Me familiariser avec les events JS et apprendre à animer des sprites.



## Aperçu 

<img src="images/link.png">